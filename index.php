<?php
	// 求める曜日（年、月、日）
	$year = 2015;
	$month = 8;
	$day = 10;

	echo get_weekday($year, $month, $day);

	// 年月日日付から曜日を求める関数
	function get_weekday($year, $month, $day) {
		$date = new DateTime();
		$date->setDate($year, $month, $day);
		$weekday = array("日", "月", "火", "水", "木", "金", "土");
		$ans = $date->format('w'); // 0 (日曜)から 6 (土曜)が数値で格納される

		return $weekday[$ans] . "曜日です。";
	}

?>